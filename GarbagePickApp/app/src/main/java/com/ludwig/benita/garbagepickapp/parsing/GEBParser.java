/*
 * This project is licensed under Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0) except as noted.
 * Copyright (c) 2019 Benita Ludwig
 *
 * All third party material is sourced in the applicable file.
 * For full license see LICENSE.txt, NOTICE.txt and LICENSE-THIRD-PARTY.txt.
 */

package com.ludwig.benita.garbagepickapp.parsing;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Calendar;

/**
 * class that parses files from GEB (Göttinger Entsorgungsbetriebe)
 * https://www.geb-goettingen.de/
 *
 * @author Benita Ludwig
 */
public class GEBParser extends IcsParser {

    /**
     * name of the WasteType PAPER in the parsed file
     */
    private static final String PAPER = "Abfuhr der Papiertonne";
    /**
     * name of the WasteType RECYCLING in the parsed file
     */
    private static final String RECYCLING = "Abfuhr des gelben Sacks";
    /**
     * name of the WasteType RESIDUAL in the parsed file
     */
    private static final String RESIDUAL = "Abfuhr der Restmülltonne";
    /**
     * name of the WasteType RESIDUAL_CONTAINER in the parsed file
     */
    private static final String RESIDUAL_CONTAINER = "Abfuhr der Container";
    /**
     * name of the WasteType ORGANIC in the parsed file
     */
    private static final String ORGANIC = "Abfuhr der Biotonne";
    /**
     * name of the WasteType LEAVES in the parsed file
     */
    private static final String LEAVES = "Baum- und Strauchschnittsammlung";
    //private static final String CHRISTMASTREE = "";

    private SharedPreferences sharedPreferences;

    /**
     * constructor that sets the Context and SharedPreferences
     *
     * @param context           Context
     * @param sharedPreferences SharedPreferences
     */
    public GEBParser(Context context, SharedPreferences sharedPreferences) {
        super(context);
        this.sharedPreferences = sharedPreferences;
        // add supported WasteTypes to allAvailableWasteTypes
        allAvailableWasteTypes.add(WasteType.PAPER);
        allAvailableWasteTypes.add(WasteType.RECYCLING);
        allAvailableWasteTypes.add(WasteType.RESIDUAL);
        allAvailableWasteTypes.add(WasteType.RESIDUAL_CONTAINER);
        allAvailableWasteTypes.add(WasteType.ORGANIC);
        allAvailableWasteTypes.add(WasteType.LEAVES);

        generateURL();
    }

    @Override
    public long downloadFile() {
        return super.downloadFile(generateURL());
    }

    @Override
    public String generateURL() {
        // get the street name from the preferences
        String street = sharedPreferences.getString("pref_addr_street", "");
        if (street == null) street = "";
        // replace all spaces with plus and shorten 'traße' to 'tr.'
        street = street.replaceAll(" ", "+").replaceAll("traße", "tr.");

        // url example: "https://www.geb-goettingen.de/abfuhr/forward.php?str=STREET&nr=1&year=2019";
        // generate the url
        String url = "https://www.geb-goettingen.de/abfuhr/forward.php?str=" + street + "&nr=" +
                sharedPreferences.getString("pref_addr_number", "") + "&year=" + Calendar.getInstance().get(Calendar.YEAR);
        // Log.d(TAG, this.getClass().getSimpleName() + ": " + "GEB URL: " + url);
        return url;
    }

    @Override
    public void generateWasteTypes() {
        // go through the WEvent list
        for (WEvent ev : super.wEventList) {
            // read the summary of the WEvent
            String summary = ev.getvEvent().getSummary().getValue();
            // check for the appearance of a WasteType name in the event and set the corresponding WasteType
            if (summary.contains(PAPER)) ev.setWasteType(WasteType.PAPER);
            if (summary.contains(RECYCLING)) ev.setWasteType(WasteType.RECYCLING);
            if (summary.contains(RESIDUAL)) ev.setWasteType(WasteType.RESIDUAL);
            if (summary.contains(RESIDUAL_CONTAINER)) ev.setWasteType(WasteType.RESIDUAL_CONTAINER);
            if (summary.contains(ORGANIC)) ev.setWasteType(WasteType.ORGANIC);
            if (summary.contains(LEAVES)) ev.setWasteType(WasteType.LEAVES);
        }
    }
}
