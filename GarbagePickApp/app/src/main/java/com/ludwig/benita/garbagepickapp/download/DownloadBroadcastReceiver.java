/*
 * This project is licensed under Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0) except as noted.
 * Copyright (c) 2019 Benita Ludwig
 *
 * All third party material is sourced in the applicable file.
 * For full license see LICENSE.txt, NOTICE.txt and LICENSE-THIRD-PARTY.txt.
 */

package com.ludwig.benita.garbagepickapp.download;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;

import com.ludwig.benita.garbagepickapp.AppFunctions;

import static com.ludwig.benita.garbagepickapp.MainActivity.TAG;

/**
 * broadcast receiver for downloads
 * <p>
 * <pre>
 * Source:
 * DownloadBroadcastReceiver.java > onReceive(final Context context, Intent intent)
 *     based on Stack Overflow: https://stackoverflow.com/q/38563474
 *         under license: CC BY-SA 3.0
 *     answer: https://stackoverflow.com/a/38563869
 *         by Sohail Zahid https://stackoverflow.com/users/4746376/sohail-zahid
 * this source was modified by Benita Ludwig
 * </pre>
 *
 * @author Benita Ludwig
 */
public class DownloadBroadcastReceiver extends BroadcastReceiver {

    /**
     * standard-constructor
     */
    public DownloadBroadcastReceiver() {
        Log.d(TAG, this.getClass().getSimpleName() + ": " + "new download broadcast receiver");
    }

    /**
     * starts the parsing in appFunctions after download was received.
     * for source see header of this file
     *
     * @param context Context
     * @param intent  Intent
     */
    @Override
    public void onReceive(final Context context, Intent intent) {
        // if download complete
        if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(intent.getAction())) {
            // check api level >= 21 (since it's only needed together with the automatic download)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                AutomaticDownloadJobIntentService.enqueueWork(context, new Intent());

            final AppFunctions appFunctions = new AppFunctions(context.getContentResolver(), context, context.getResources());
            final boolean isAutomatic = PreferenceManager.getDefaultSharedPreferences(context).getBoolean("isAutomatic", false);
            long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
            Log.d(TAG, this.getClass().getSimpleName() + ": " + "download received " + downloadId);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    appFunctions.parseFile(context, false, isAutomatic);
                }
            }).start();
        }
    }
}
