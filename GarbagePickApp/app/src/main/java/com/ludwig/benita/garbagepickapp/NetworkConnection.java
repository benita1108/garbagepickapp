/*
 * This project is licensed under Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0) except as noted.
 * Copyright (c) 2019 Benita Ludwig
 *
 * All third party material is sourced in the applicable file.
 * For full license see LICENSE.txt, NOTICE.txt and LICENSE-THIRD-PARTY.txt.
 */

package com.ludwig.benita.garbagepickapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.widget.Toast;

/**
 * class that checks the current network connection
 * <p>
 * <pre>
 * Source:
 * NetworkConnection.java > connectedWifi()
 *     taken from Stack Overflow: https://stackoverflow.com/q/3841317
 *         under license: CC BY-SA 3.0
 *     answer: https://stackoverflow.com/a/34904367
 *         by revolutionary https://stackoverflow.com/users/565223/revolutionary
 * this source was modified by Benita Ludwig
 * </pre>
 *
 * @author Benita Ludwig
 */
public class NetworkConnection {

    private Context context;

    /**
     * constructor that sets the Context
     *
     * @param context Context
     */
    public NetworkConnection(Context context) {
        this.context = context;
    }

    /**
     * checks all necessary connections for a download
     *
     * @param networkMobile true if downloads via mobile data are allowed in the preferences, else false
     * @param networkWifi   true if downloads via wifi are allowed in the preferences, else false
     * @return true if current connections are available, else false
     */
    boolean checkConnection(boolean networkMobile, boolean networkWifi) {
        // mobile and wifi not allowed
        if (!networkMobile && !networkWifi) {
            // can't download without allowed network connection
            Toast.makeText(context, context.getResources().getString(R.string.noNetworkPref), Toast.LENGTH_LONG).show();
            return false;
        }
        // mobile and wifi allowed
        if (networkMobile && networkWifi) {
            // currently not connected to mobile and wifi
            if (!this.connectedMobile() && !this.connectedWifi()) {
                // can't download without connected network
                Toast.makeText(context, context.getResources().getString(R.string.noNetworkConnection), Toast.LENGTH_LONG).show();
                return false;
            } else return true; // both are allowed and at least one of them is connected
        }
        // mobile is allowed but not connected
        if (networkMobile && !this.connectedMobile()) {
            // can't download without connected network
            Toast.makeText(context, context.getResources().getString(R.string.noMobileConnection), Toast.LENGTH_LONG).show();
            return false;
        }
        // wifi is allowed but not connected
        if (networkWifi && !this.connectedWifi()) {
            // can't download without connected network
            Toast.makeText(context, context.getResources().getString(R.string.noWifiConnection), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    /**
     * checks if the device is currently connected to a wifi network.
     * for source see header of this file
     *
     * @return true if connected, else false
     */
    public boolean connectedWifi() {
        WifiManager wifiMgr = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (wifiMgr.isWifiEnabled()) { // WiFi adapter is ON
            return wifiMgr.getConnectionInfo().getNetworkId() != -1;
        } else {
            return false; // WiFi adapter is OFF
        }
    }

    /**
     * checks if the device is currently connected to a mobile network
     *
     * @return true if connected, else false
     */
    private boolean connectedMobile() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if (activeNetwork == null) return false; // no active network
        return activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE && activeNetwork.isConnected();
    }
}
