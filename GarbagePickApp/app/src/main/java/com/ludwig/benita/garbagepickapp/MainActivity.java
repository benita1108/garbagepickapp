/*
 * This project is licensed under Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0) except as noted.
 * Copyright (c) 2019 Benita Ludwig
 *
 *
 * 2-Clause BSD License for Biweekly:
 *  Copyright (c) 2013-2018, Michael Angstadt
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice, this
 *  list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 *  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * All third party material is sourced in the applicable file.
 * For full license see LICENSE.txt, NOTICE.txt and LICENSE-THIRD-PARTY.txt.
 *
 * Every copy, redistribution or adaption of this project or parts of it must
 * include this NOTICE file and every copyright and license that is compiled
 * in the LICENSE-THIRD-PARTY file.
 */

package com.ludwig.benita.garbagepickapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.ludwig.benita.garbagepickapp.preferences.SettingsActivity;

/**
 * main activity of the app
 * <p>
 * <pre>
 * This project uses the library biweekly and its example code by Michael Angstadt.
 * https://github.com/mangstadt/biweekly
 *     under license: BSD 2-Clause
 * Copyright (c) 2013-2018, Michael Angstadt
 *
 * Source:
 * MainActivity.java > onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults)
 *     based on tutorial by Android Developers: https://developer.android.com/training/permissions/requesting#handle-response
 *         under license: Apache 2.0
 * this source was modified by Benita Ludwig
 *
 * MainActivity.java > onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key),
 * registerPreferenceListener(), unregisterPreferenceListener()
 *     taken from Stack Overflow: https://stackoverflow.com/q/21690508
 *         under license: CC BY-SA 3.0
 *     answer: https://stackoverflow.com/a/21690871
 *         by kcoppock https://stackoverflow.com/users/321697/kcoppock
 * this source was modified by Benita Ludwig
 * </pre>
 *
 * @author Benita Ludwig
 */
public class MainActivity extends AppCompatActivity
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    /**
     * tag for logging
     */
    public static final String TAG = "GarbagePickApp";
    /**
     * callback code for the calendar permission
     */
    private static final int CALLBACK_CALENDAR = 24081;
    /**
     * callback code for the storage permission
     */
    private static final int CALLBACK_STORAGE = 24082;

    private AppFunctions appFunctions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button addEventsBtn = findViewById(R.id.btnEvents);
        addEventsBtn.setEnabled(true);
        addEventsBtn.setText(getResources().getString(R.string.btnEvents));

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
            actionBar.setTitle(getResources().getString(R.string.calendar_display_name));

        appFunctions = new AppFunctions(getContentResolver(), this, getResources());

        appFunctions.setMainActivityContext(this);
        appFunctions.setAddEventsBtn(addEventsBtn);

        registerPreferenceListener();
    }

    /**
     * on-click method for the "Add Events" button.
     * adds events to the calendar
     *
     * @param v View
     */
    public void onAddEvents(View v) {
        appFunctions.addEvents(false, true);
    }

    /**
     * on-click method for the "Open Calendar" button.
     * opens the calendar application
     *
     * @param v View
     */
    public void onCalendarOpen(View v) {
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("content://com.android.calendar/time/" + System.currentTimeMillis()));
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_settings:
                Intent settingsIntent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(settingsIntent);
                break;
            case R.id.menu_information:
                Intent infoIntent = new Intent(getApplicationContext(), InformationActivity.class);
                startActivity(infoIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        appFunctions.setAddEventsBtn(null);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterPreferenceListener();
    }


    /**
     * prints the permission state.
     * for source see header of this file
     *
     * @param requestCode  request code
     * @param permissions  permission
     * @param grantResults grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case CALLBACK_CALENDAR: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, this.getClass().getSimpleName() + ": " + "CALENDAR PERMISSION GRANTED");
                    //getPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, CALLBACK_STORAGE);
                } else {
                    Log.d(TAG, this.getClass().getSimpleName() + ": " + "CALENDAR PERMISSION DENIED");
                }
            }
            case CALLBACK_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, this.getClass().getSimpleName() + ": " + "STORAGE PERMISSION GRANTED");
                } else {
                    Log.d(TAG, this.getClass().getSimpleName() + ": " + "STORAGE PERMISSION DENIED");
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * notices changes in the preferences and saves them in changedPreference.
     * for source see header of this file
     *
     * @param sharedPreferences shared preference
     * @param key               key of the shared preference
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        String currentPref = sharedPreferences.getString("changedPreference", "");
        switch (key) {
            case "pref_company":
                //Log.d(TAG, this.getClass().getSimpleName() + ": " + "company changed");
                sharedPreferences.edit().putString("changedPreference", currentPref + " company address").apply();
                break;
            case "pref_addr_street":
            case "pref_addr_number":
            case "pref_addr_district":
            case "pref_addr_city":
                //Log.d(TAG, this.getClass().getSimpleName() + ": " + "address changed");
                sharedPreferences.edit().putString("changedPreference", currentPref + " address").apply();
                break;
            case "pref_notifs":
            case "pref_notifs_days":
            case "pref_notifs_time":
            case "pref_notifs_repeat":
                //Log.d(TAG, this.getClass().getSimpleName() + ": " + "notifications changed");
                sharedPreferences.edit().putString("changedPreference", currentPref + " notifs").apply();
                break;
            case "pref_downloads":
                //Log.d(TAG, this.getClass().getSimpleName() + ": " + "automatic downloads changed");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    sharedPreferences.edit().putString("changedPreference", currentPref + " downloads").apply();
                } else {
                    Toast.makeText(this, getResources().getString(R.string.automaticUpdatesVersion), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    /**
     * registers the PreferenceListener.
     * for source see header of this file
     */
    private void registerPreferenceListener() {
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
    }

    /**
     * unregisters the PreferenceListener.
     * for source see header of this file
     */
    private void unregisterPreferenceListener() {
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
    }
}

