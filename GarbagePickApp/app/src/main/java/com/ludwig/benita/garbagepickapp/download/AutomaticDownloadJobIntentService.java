/*
 * This project is licensed under Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0) except as noted.
 * Copyright (c) 2019 Benita Ludwig
 *
 * All third party material is sourced in the applicable file.
 * For full license see LICENSE.txt, NOTICE.txt and LICENSE-THIRD-PARTY.txt.
 */

package com.ludwig.benita.garbagepickapp.download;

import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.util.Log;

import static com.ludwig.benita.garbagepickapp.MainActivity.TAG;

/**
 * implementation of JobIntentService to broadcast a DownloadBroadcastReceiver
 * <p>
 * <pre>
 * Source:
 * AutomaticDownloadJobIntentService.java
 *     taken from Android Developers: https://developer.android.com/reference/android/support/v4/app/JobIntentService
 *         under license: Apache 2.0
 * this source was modified by Benita Ludwig
 * </pre>
 */
public class AutomaticDownloadJobIntentService extends JobIntentService {
    /**
     * Unique job ID for this service.
     */
    static final int JOB_ID = 1000;

    /**
     * Convenience method for enqueuing work in to this service.
     */
    static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, AutomaticDownloadJobIntentService.class, JOB_ID, work);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        // We have received work to do. The system or framework is already
        // holding a wake lock for us at this point, so we can just go.
        Log.d(TAG, this.getClass().getSimpleName() + ": " + "onhandlework(): send broadcast");
        Intent broadcastIntent = new Intent(this, DownloadBroadcastReceiver.class);
        PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean("isAutomatic", true).apply();
        sendBroadcast(broadcastIntent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Log.d(TAG, this.getClass().getSimpleName() + ": " + "ondestroy()");
    }
}