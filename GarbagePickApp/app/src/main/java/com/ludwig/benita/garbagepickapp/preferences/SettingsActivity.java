package com.ludwig.benita.garbagepickapp.preferences;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.ludwig.benita.garbagepickapp.AppFunctions;
import com.ludwig.benita.garbagepickapp.CalendarFunctions;
import com.ludwig.benita.garbagepickapp.R;
import com.ludwig.benita.garbagepickapp.download.AutomaticDownloadJobService;
import com.ludwig.benita.garbagepickapp.parsing.WasteType;

import java.util.List;

import static com.ludwig.benita.garbagepickapp.MainActivity.TAG;

/**
 * A PreferenceActivity that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends AppCompatPreferenceActivity {

    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();

            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);

                // Set the summary to reflect the new value.
                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);
            } else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.setSummary(stringValue);
            }
            return true;
        }

    };
    private Context context;

    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();
        context = getApplicationContext();
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //finish();
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * executes the respective functions if a preference was changed when the setting screen is destroyed
     */
    @Override
    protected void onDestroy() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        // check if a preference was changed by reading the value of changedPreference
        String changedPreference = sharedPreferences.getString("changedPreference", "");
        if (changedPreference == null) changedPreference = "";

        ActionBar actionBar = getSupportActionBar();
        // only update the events when the main settings are left and all changes were made
        CharSequence actionBarTitle = actionBar.getTitle();
        if (actionBarTitle == null) actionBarTitle = "";
        if (actionBarTitle.toString().contains(getResources().getString(R.string.settings))) {
            Log.d(TAG, this.getClass().getSimpleName() + ": " + "main settings screen was destroyed -> apply changes to the events if some were made");
            AppFunctions appFunctions;
            if (changedPreference.contains("company")) {
                // company was changed -> delete all calendars from the old company
                Log.d(TAG, this.getClass().getSimpleName() + ": " + "changed company");
                for (WasteType wasteType : WasteType.values())
                    (new CalendarFunctions(context.getContentResolver(), context)).deleteCalendar(wasteType);
            }
            if (changedPreference.contains("downloads")) {
                // automatic download interval was changed -> new job includes an download so there's no need to check for the other changes
                Log.d(TAG, this.getClass().getSimpleName() + ": " + "changed download interval");
                // reset the changedPreference value
                sharedPreferences.edit().putString("changedPreference", "").apply();
                // read the new value for the download interval
                String downloadInterval = sharedPreferences.getString("pref_downloads", "-1");
                if (downloadInterval == null) downloadInterval = "-1";
                int automaticDownloadInterval = Integer.parseInt(downloadInterval);
                Toast.makeText(context, getResources().getString(R.string.updating), Toast.LENGTH_LONG).show();
                // start a new AutomaticDownloadJobService
                new AutomaticDownloadJobService(this).setAutomaticDownload(automaticDownloadInterval);
            } else {
                // download interval wasn't changed -> check which preference was changed to decide on update-function:
                // if only the notifications and not the address were changed
                if (changedPreference.contains("notifs") && !changedPreference.contains("address")) {
                    // notifications were changed -> updateEventNotifications()
                    Log.d(TAG, this.getClass().getSimpleName() + ": " + "changed notifications");
                    // reset the changedPreference value
                    sharedPreferences.edit().putString("changedPreference", "").apply();
                    Toast.makeText(context, getResources().getString(R.string.updating), Toast.LENGTH_LONG).show();
                    appFunctions = new AppFunctions(this.getContentResolver(), this.getApplicationContext(), this.getResources());
                    appFunctions.updateEventNotifications();
                } else
                    // address was changed -> re-download the file with the new address via addEvents()
                    if (changedPreference.contains("address")) {
                        Log.d(TAG, this.getClass().getSimpleName() + ": " + "changed address");
                        // reset the changedPreference value
                        sharedPreferences.edit().putString("changedPreference", "").apply();
                        Toast.makeText(context, getResources().getString(R.string.updating), Toast.LENGTH_LONG).show();
                        appFunctions = new AppFunctions(this.getContentResolver(), this.getApplicationContext(), this.getResources());
                        appFunctions.addEvents(false, true);
                    }
            }
        }
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.pref_headers, target);
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || NotificationPreferenceFragment.class.getName().equals(fragmentName)
                || AddressPreferenceFragment.class.getName().equals(fragmentName)
                || NetworkPreferenceFragment.class.getName().equals(fragmentName);
    }

    /**
     * deletes all events from the calendars
     *
     * @param view View
     */
    public void onDeleteAllEvents(View view) {
        Log.d(TAG, this.getClass().getSimpleName() + ": " + "DELETE ALL EVENTS");
        // go through all WasteTypes and delete all events of each calendar
        for (WasteType wasteType : WasteType.values()) {
            (new CalendarFunctions(getContentResolver(), getApplicationContext())).deleteAllEvents(wasteType);
        }
        Toast.makeText(context, getResources().getString(R.string.deletedEvents), Toast.LENGTH_LONG).show();
    }

    /**
     * This fragment shows notification preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class NotificationPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_notification);
            setHasOptionsMenu(true);

            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.
            bindPreferenceSummaryToValue(findPreference("pref_notifs_days"));
            bindPreferenceSummaryToValue(findPreference("pref_notifs_repeat"));
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }

    /**
     * This fragment shows address preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class AddressPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_address);
            setHasOptionsMenu(true);

            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.
            bindPreferenceSummaryToValue(findPreference("pref_company"));
            bindPreferenceSummaryToValue(findPreference("pref_addr_city"));
            bindPreferenceSummaryToValue(findPreference("pref_addr_district"));
            bindPreferenceSummaryToValue(findPreference("pref_addr_street"));
            bindPreferenceSummaryToValue(findPreference("pref_addr_number"));
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }

    /**
     * This fragment shows network preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class NetworkPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_network);
            setHasOptionsMenu(true);
            bindPreferenceSummaryToValue(findPreference("pref_downloads"));
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }
}
