/*
 * This project is licensed under Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0) except as noted.
 * Copyright (c) 2019 Benita Ludwig
 *
 * All third party material is sourced in the applicable file.
 * For full license see LICENSE.txt, NOTICE.txt and LICENSE-THIRD-PARTY.txt.
 */

package com.ludwig.benita.garbagepickapp.parsing;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.ludwig.benita.garbagepickapp.R;

/**
 * enum that provides all information for the waste types
 *
 * @author Benita Ludwig
 */
public enum WasteType {
    /**
     * paper and cardboard waste
     */
    PAPER,
    /**
     * recyclable waste
     */
    RECYCLING,
    /**
     * garbage/residual waste that can't be recycled
     */
    RESIDUAL,
    /**
     * garbage/residual waste that can't be recycled. for the container as specified by GEB
     */
    RESIDUAL_CONTAINER,
    /**
     * compostables and organic waste
     */
    ORGANIC,
    /**
     * collection of christmas trees
     */
    CHRISTMASTREE,
    /**
     * yard waste like leaves and tree cuttings
     */
    LEAVES,
    /**
     * default
     */
    DEFAULT;

    /**
     * returns the title of a given WasteType
     *
     * @param wasteType WasteType
     * @param context   Context
     * @return title of the WasteType
     */
    public static String getTitle(WasteType wasteType, Context context) {
        switch (wasteType) {
            case PAPER:
                return context.getResources().getString(R.string.titlePaper);
            case RECYCLING:
                return context.getResources().getString(R.string.titleRecycling);
            case RESIDUAL:
                return context.getResources().getString(R.string.titleResidual);
            case RESIDUAL_CONTAINER:
                return context.getResources().getString(R.string.titleResidualContainer);
            case ORGANIC:
                return context.getResources().getString(R.string.titleOrganic);
            case CHRISTMASTREE:
                return context.getResources().getString(R.string.titleChristmastree);
            case LEAVES:
                return context.getResources().getString(R.string.titleLeaves);
            case DEFAULT:
                return context.getResources().getString(R.string.titleOther);
        }
        return context.getResources().getString(R.string.titleOther);
    }

    /**
     * returns the description of a given WasteType
     *
     * @param wasteType WasteType
     * @param context   Context
     * @return description of the WasteType
     */
    public static String getDesc(WasteType wasteType, Context context) {
        switch (wasteType) {
            case PAPER:
                return context.getResources().getString(R.string.descPaper);
            case RECYCLING:
                return context.getResources().getString(R.string.descRecycling);
            case RESIDUAL:
                return context.getResources().getString(R.string.descResidual);
            case RESIDUAL_CONTAINER:
                return context.getResources().getString(R.string.descResidualContainer);
            case ORGANIC:
                return context.getResources().getString(R.string.descOrganic);
            case CHRISTMASTREE:
                return context.getResources().getString(R.string.descChristmastree);
            case LEAVES:
                return context.getResources().getString(R.string.descLeaves);
            case DEFAULT:
                return context.getResources().getString(R.string.descOther);
        }
        return context.getResources().getString(R.string.descOther);
    }

    /**
     * returns the color of a given WasteType as an int
     *
     * @param wasteType WasteType
     * @param context   Context
     * @return color of the WasteType
     */
    public static int getColor(WasteType wasteType, Context context) {
        switch (wasteType) {
            case PAPER:
                return ContextCompat.getColor(context, R.color.colorPaper);
            case RECYCLING:
                return ContextCompat.getColor(context, R.color.colorRecycling);
            case RESIDUAL:
                return ContextCompat.getColor(context, R.color.colorResidual);
            case RESIDUAL_CONTAINER:
                return ContextCompat.getColor(context, R.color.colorResidual);
            case ORGANIC:
                return ContextCompat.getColor(context, R.color.colorOrganic);
            case CHRISTMASTREE:
                return ContextCompat.getColor(context, R.color.colorChristmastree);
            case LEAVES:
                return ContextCompat.getColor(context, R.color.colorLeaves);
            case DEFAULT:
                return ContextCompat.getColor(context, R.color.colorOther);
        }
        return ContextCompat.getColor(context, R.color.colorOther);
    }
}