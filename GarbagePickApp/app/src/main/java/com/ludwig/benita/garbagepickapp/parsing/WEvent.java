/*
 * This project is licensed under Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0) except as noted.
 * Copyright (c) 2019 Benita Ludwig
 *
 * All third party material is sourced in the applicable file.
 * For full license see LICENSE.txt, NOTICE.txt and LICENSE-THIRD-PARTY.txt.
 */

package com.ludwig.benita.garbagepickapp.parsing;

import android.os.Build;

import java.util.Calendar;
import java.util.TimeZone;

import biweekly.component.VEvent;

/**
 * class that acts as a wrapper for a VEvent to store additional information about the event that
 * is needed to correctly display the events in the calendar.
 * <p>
 * <pre>
 * This project uses the library biweekly and its example code by Michael Angstadt.
 * https://github.com/mangstadt/biweekly
 *     under license: BSD 2-Clause
 * Copyright (c) 2013-2018, Michael Angstadt
 * </pre>
 *
 * @author Benita Ludwig
 */
public class WEvent {
    /**
     * utc offset of the event in millis
     */
    private static long utcOffsetInMillis = 0;
    /**
     * year of the event
     */
    private int year;
    /**
     * month of the event
     */
    private int month;
    /**
     * day of the event
     */
    private int day;
    /**
     * time of the event in millis
     */
    private long time;
    /**
     * hour of the event
     */
    private int hour;
    /**
     * minute of the event
     */
    private int min;
    /**
     * WasteType of the event
     */
    private WasteType wasteType;
    /**
     * VEvent-object of the event
     */
    private VEvent vEvent;

    /**
     * constructor that sets the VEvent
     *
     * @param event VEvent
     */
    WEvent(VEvent event) {
        vEvent = event;
        // timestamp of the event start in millis
        time = event.getDateStart().getValue().getTime();

        // check if event has timezone for calendar
        Calendar cal;
        if (event.getDateStart().getValue().hasTime()) {
            // check api level >= 24
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                time += utcOffsetInMillis;
            cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        } else {
            cal = Calendar.getInstance();
        }
        cal.setTimeInMillis(time);
        // get components from Calendar-object
        year = cal.get(Calendar.YEAR);
        month = cal.get(Calendar.MONTH);
        day = cal.get(Calendar.DATE);
        hour = cal.get(Calendar.HOUR_OF_DAY);
        min = cal.get(Calendar.MINUTE);

        wasteType = WasteType.DEFAULT;
    }

    /**
     * sets the utc offset of the event in millis
     *
     * @param utcOffsetInMillis utc offset of the event in millis
     */
    static void setUtcOffsetInMillis(long utcOffsetInMillis) {
        WEvent.utcOffsetInMillis = utcOffsetInMillis;
    }

    /**
     * gets the year of the event
     * @return year of the event
     */
    public int getYear() {
        return year;
    }

    /**
     * gets the month of the event
     * @return month of the event
     */
    public int getMonth() {
        return month;
    }

    /**
     * gets the day of the event
     * @return day of the event
     */
    public int getDay() {
        return day;
    }

    /**
     * gets the timestamp from the start of the event in millis
     * @return timestamp from the start of the event in millis
     */
    public long getTime() {
        return time;
    }

    /**
     * gets the hour of the event
     * @return hour of the event
     */
    public int getHour() {
        return hour;
    }

    /**
     * gets the minute of the event
     * @return minute of the event
     */
    public int getMin() {
        return min;
    }

    /**
     * gets the WasteType of the event
     * @return WasteType of the event
     */
    public WasteType getWasteType() {
        return wasteType;
    }

    /**
     * sets the WasteType of the event
     *
     * @param wasteType WasteType of the event
     */
    void setWasteType(WasteType wasteType) {
        this.wasteType = wasteType;
    }

    /**
     * gets the VEvent-object of the event
     * @return VEvent-object of the event
     */
    public VEvent getvEvent() {
        return vEvent;
    }
}
