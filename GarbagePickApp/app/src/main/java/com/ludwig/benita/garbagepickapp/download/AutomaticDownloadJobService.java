/*
 * This project is licensed under Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0) except as noted.
 * Copyright (c) 2019 Benita Ludwig
 *
 * All third party material is sourced in the applicable file.
 * For full license see LICENSE.txt, NOTICE.txt and LICENSE-THIRD-PARTY.txt.
 */

package com.ludwig.benita.garbagepickapp.download;

import android.annotation.TargetApi;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.ludwig.benita.garbagepickapp.AppFunctions;

import static com.ludwig.benita.garbagepickapp.MainActivity.TAG;

/**
 * class that provides the JobService for automatic downloads
 *
 * @author Benita Ludwig
 */
@TargetApi(21)
public class AutomaticDownloadJobService extends JobService {

    /**
     * preference for downloads via mobile data
     */
    private static boolean networkMobile = false;
    /**
     * preference for downloads via wifi
     */
    private static boolean networkWifi = true;

    private Context context;

    /**
     * standard-constructor
     */
    public AutomaticDownloadJobService() {
        super();
    }

    /**
     * constructor that sets the Context
     *
     * @param context Context
     */
    public AutomaticDownloadJobService(Context context) {
        this.context = context;
    }

    /**
     * sets network connection via mobile data
     *
     * @param networkMobile true if downloads via mobile data are allowed, else false
     */
    public static void setNetworkMobile(boolean networkMobile) {
        AutomaticDownloadJobService.networkMobile = networkMobile;
    }

    /**
     * sets network connection via wifi
     *
     * @param networkWifi true if downloads via wifi are allowed, else false
     */
    public static void setNetworkWifi(boolean networkWifi) {
        AutomaticDownloadJobService.networkWifi = networkWifi;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        Log.d(TAG, this.getClass().getSimpleName() + ": " + "automatic download started");
        AppFunctions appFunctions = new AppFunctions(getContentResolver(), getApplicationContext(), getResources());
        // automatically add events
        appFunctions.addEvents(true, true);
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        //Log.d(TAG, this.getClass().getSimpleName() + ": " + "automatic download stopped");
        return false;
    }

    /**
     * sets the automatic download by creating a new JobService
     *
     * @param automaticDownloadInterval interval for the automatic downloads in days
     */
    public void setAutomaticDownload(int automaticDownloadInterval) {
        // check api level >= 21
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //Log.d(TAG, this.getClass().getSimpleName() + ": " + "automatic download every " + automaticDownloadInterval + " days");

            // create JobScheduler
            JobScheduler jobScheduler = (JobScheduler) context.getSystemService(JOB_SCHEDULER_SERVICE);
            // cancel all previous jobs
            jobScheduler.cancelAll();

            if (automaticDownloadInterval > 2) {
                // calculate time = 1 day * automaticDownloadInterval [in days] from the user preference
                long time = 86400000L /*day*/ * automaticDownloadInterval;

                // repeat job periodically even after reboot
                JobInfo.Builder jobInfoBuilder = new JobInfo.Builder(1, new ComponentName(context, AutomaticDownloadJobService.class))
                        .setPeriodic(time).setPersisted(true);
                // add required network types depending on the user preference
                if (networkMobile && !networkWifi)
                    jobInfoBuilder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_CELLULAR);
                if (networkWifi)
                    jobInfoBuilder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED);
                // schedule the job
                jobScheduler.schedule(jobInfoBuilder.build());
            }
        }
    }
}