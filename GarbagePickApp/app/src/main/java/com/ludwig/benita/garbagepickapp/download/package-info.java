/**
 * package that contains the classes for downloading new event files
 *
 * @author Benita Ludwig
 */
package com.ludwig.benita.garbagepickapp.download;
