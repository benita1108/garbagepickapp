/**
 * package that contains all classes for the settings/preference activity
 *
 * @author Benita Ludwig
 */
package com.ludwig.benita.garbagepickapp.preferences;
