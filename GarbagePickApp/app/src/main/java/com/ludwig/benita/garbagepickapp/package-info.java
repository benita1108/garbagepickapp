/**
 * package that contains the main function classes for the app and the calendar,
 * the main activity and the information activity
 *
 * @author Benita Ludwig
 */
package com.ludwig.benita.garbagepickapp;
