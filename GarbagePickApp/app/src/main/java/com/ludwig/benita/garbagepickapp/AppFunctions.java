/*
 * This project is licensed under Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0) except as noted.
 * Copyright (c) 2019 Benita Ludwig
 *
 * All third party material is sourced in the applicable file.
 * For full license see LICENSE.txt, NOTICE.txt and LICENSE-THIRD-PARTY.txt.
 */

package com.ludwig.benita.garbagepickapp;

import android.Manifest;
import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.ludwig.benita.garbagepickapp.download.AutomaticDownloadJobService;
import com.ludwig.benita.garbagepickapp.parsing.GEBParser;
import com.ludwig.benita.garbagepickapp.parsing.IcsParser;
import com.ludwig.benita.garbagepickapp.parsing.SBAZVParser;
import com.ludwig.benita.garbagepickapp.parsing.WEvent;
import com.ludwig.benita.garbagepickapp.parsing.WasteType;
import com.ludwig.benita.garbagepickapp.preferences.SettingsActivity;

import java.io.File;

import biweekly.component.VEvent;

import static com.ludwig.benita.garbagepickapp.MainActivity.TAG;
import static com.ludwig.benita.garbagepickapp.parsing.IcsParser.FILENAME;

/**
 * class that provides all the functions related to the app
 * <p>
 * <pre>
 * This project uses the library biweekly and its example code by Michael Angstadt.
 * https://github.com/mangstadt/biweekly
 *     under license: BSD 2-Clause
 * Copyright (c) 2013-2018, Michael Angstadt
 *
 * Source:
 * AppFunctions.java > automaticUpdateNotification(int numberOfUpdatedEvents)
 *     based on tutorial by Android Developers: https://developer.android.com/training/notify-user/build-notification#SimpleNotification
 *         under license: Apache 2.0
 * this source was modified by Benita Ludwig
 *
 * AppFunctions.java > createNotificationChannel()
 *     taken from Android Developers: https://developer.android.com/training/notify-user/build-notification#Priority
 *         under license: Apache 2.0
 * </pre>
 *
 * @author Benita Ludwig
 */
public class AppFunctions {
    /**
     * callback code for the calendar permission
     */
    private static final int CALLBACK_CALENDAR = 24081;
    // callback code for the storage permission
    //private static final int CALLBACK_STORAGE = 24082;
    /**
     * context of the MainActivity to update the "Add Events" button
     */
    private static Context mainActivityContext;
    /**
     * button for "Add Events"
     */
    private static Button addEventsBtn;
    /**
     * id of the notification channel
     */
    private final String CHANNEL_ID = "Garbage Pick-App Notification Channel";
    /**
     * preference for downloads via mobile data
     */
    private boolean networkMobile = false;
    /**
     * preference for downloads via wifi
     */
    private boolean networkWifi = true;
    /**
     * preference to show notifications
     */
    private boolean notification = false;

    private CalendarFunctions calendarFunctions;
    private IcsParser parser;
    private SharedPreferences sharedPreferences;
    private Context context;
    private ContentResolver resolver;
    private Resources resources;

    /**
     * constructor that sets the ContentResolver, Context and Resources
     *
     * @param resolver  ContentResolver
     * @param context   Context
     * @param resources Resources
     */
    public AppFunctions(ContentResolver resolver, Context context, Resources resources) {
        this.resolver = resolver;
        this.context = context;
        this.resources = resources;

        initApp();
    }

    /**
     * sets the MainActivityContext
     *
     * @param mainActivityContext MainActivityContext
     */
    void setMainActivityContext(Context mainActivityContext) {
        AppFunctions.mainActivityContext = mainActivityContext;
    }

    /**
     * sets button to add events
     *
     * @param addEventsBtn Button
     */
    void setAddEventsBtn(Button addEventsBtn) {
        AppFunctions.addEventsBtn = addEventsBtn;
    }

    /**
     * initializes all necessary parameters to use AppFunctions
     */
    private void initApp() {

        // permission for calendar
        if (!getPermission(Manifest.permission.WRITE_CALENDAR, CALLBACK_CALENDAR)) {
            Toast.makeText(context, resources.getString(R.string.permissionCalendar), Toast.LENGTH_LONG).show();
        }
        // permission for external storage (not needed anymore since the app's private external storage directory
        // is used, which doesn't need the user's permission beginning with api level 19)
        /* if (!getPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, CALLBACK_STORAGE)) {
            Toast.makeText(context, getResources().getString(R.string.permissionStorage), Toast.LENGTH_LONG).show();
        }*/

        calendarFunctions = new CalendarFunctions(resolver, context);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        // create the right parser depending on the user's preference
        String company = sharedPreferences.getString("pref_company", "sbazv");
        createParser(company);

        // check for calendars if all the permissions are granted
        if (getPermission(Manifest.permission.READ_CALENDAR, CALLBACK_CALENDAR)
                && getPermission(Manifest.permission.WRITE_CALENDAR, CALLBACK_CALENDAR)) {
            checkForCalendars();
        }
    }

    /**
     * creates the parser according to the user's preference
     *
     * @param company the user's choice for the waste management
     */
    private void createParser(String company) {
        if (company == null) company = "sbazv";
        switch (company) {
            case "geb":
                parser = new GEBParser(context, sharedPreferences);
                break;
            case "sbazv":
            default:
                parser = new SBAZVParser(context, sharedPreferences);
                break;
        }
    }

    /**
     * checks if needed calendars for the waste management are already there, else they're created
     */
    private void checkForCalendars() {
        for (WasteType wasteType : parser.getAllAvailableWasteTypes()) {
            if (calendarFunctions.getCalendarId(wasteType) == -1) {
                Log.d(TAG, this.getClass().getSimpleName() + ": " + "create calendar for " + wasteType.toString());
                calendarFunctions.createCalendar(wasteType);
            }
        }
    }

    /**
     * updates the button "Add Events" on the MainActivity
     *
     * @param enabled true if the button should be enable, else false
     * @param text    text displayed on the button
     */
    private void updateAddEventsBtn(boolean enabled, String text) {
        if (addEventsBtn != null) {
            addEventsBtn.setEnabled(enabled);
            addEventsBtn.setText(text);
        }
    }

    /**
     * starts the process of adding events to the calendar
     *
     * @param isAutomatic      true if the method is called via the automatic download, false if the method is started by the user
     * @param includesDownload true if the method should also download a new file, false else
     */
    public void addEvents(boolean isAutomatic, boolean includesDownload) {

        Log.d(TAG, this.getClass().getSimpleName() + ": " + "add events");

        updateAddEventsBtn(false, resources.getString(R.string.updating));

        // cancel preparation if the permissions are not granted
        if (!getPermission(Manifest.permission.WRITE_CALENDAR, CALLBACK_CALENDAR) || !getPermission(Manifest.permission.READ_CALENDAR, CALLBACK_CALENDAR)) {
            Toast.makeText(context, resources.getString(R.string.permissionCalendar), Toast.LENGTH_LONG).show();
            updateAddEventsBtn(true, resources.getString(R.string.btnEvents));
            return;
        }
        // permission for external storage (not needed anymore since the app's private external storage directory
        // is used, which doesn't need the user's permission beginning with api level 19)
        /* if (!getPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, CALLBACK_STORAGE)) {
            Toast.makeText(context, getResources().getString(R.string.permissionStorage), Toast.LENGTH_LONG).show();
            return;
        }*/

        checkForCalendars();
        // delete all previous events from the calendars
        for (WasteType wasteType : WasteType.values())
            calendarFunctions.deleteAllEvents(wasteType);

        readPreferences();

        if (includesDownload) {
            // check for necessary network connections
            NetworkConnection nwc = new NetworkConnection(context);
            if (!nwc.checkConnection(networkMobile, networkWifi)) {
                updateAddEventsBtn(true, resources.getString(R.string.btnEvents));
                // stop process if there's no necessary network connection
                return;
            }

            final long download = parser.downloadFile();
            if (download == -1) {
                updateAddEventsBtn(true, resources.getString(R.string.btnEvents));
            } else {
                PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("isAutomatic", isAutomatic).apply();
            }
        }
    }

    /**
     * reads the preferences and stores the values in this class
     */
    private void readPreferences() {
        String address = sharedPreferences.getString("pref_addr_street", "") + " " +
                sharedPreferences.getString("pref_addr_number", "") + " " +
                sharedPreferences.getString("pref_addr_district", "") + " " +
                sharedPreferences.getString("pref_addr_city", "");
        calendarFunctions.setAddress(address);


        notification = sharedPreferences.getBoolean("pref_notifs", false);
        // passes the notification to the CalendarFunctions class
        calendarFunctions.setNotifications(notification);

        String date = sharedPreferences.getString("pref_notifs_days", "0");
        if (date == null) date = "0";
        calendarFunctions.setNotificationDaysAhead(Integer.parseInt(date));
        String time = sharedPreferences.getString("pref_notifs_time", "0");
        if (time == null) time = "0";
        calendarFunctions.setNotificationTime(Long.parseLong(time));
        String repeat = sharedPreferences.getString("pref_notifs_repeat", "-1");
        if (repeat == null) repeat = "-1";
        calendarFunctions.setNotificationRepeat(Long.parseLong(repeat));

        networkMobile = sharedPreferences.getBoolean("pref_network_mobile", false);
        networkWifi = sharedPreferences.getBoolean("pref_network_wifi", true);
        if (parser != null) {
            // passes the network preferences to the parser
            parser.setNetworkMobile(networkMobile);
            parser.setNetworkWifi(networkWifi);
        }
        // check api level >= 21
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // passes the network preferences to the AutomaticDownloadJobService
            AutomaticDownloadJobService.setNetworkMobile(networkMobile);
            AutomaticDownloadJobService.setNetworkWifi(networkWifi);
        }
    }

    /**
     * updates the notifications after a user changed the preferences
     */
    public void updateEventNotifications() {
        final File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), FILENAME);
        new Thread(new Runnable() {
            @Override
            public void run() {
                // check if file still exists
                if (!file.exists()) {
                    Log.d(TAG, "AppFunctions: " + "update notifications: download file again");
                    // file doesn't exist anymore -> re-download the file and add the events with updated notifications again
                    addEvents(false, true);
                } else {
                    Log.d(TAG, "AppFunctions: " + "update notifications: file still exists");
                    // file still exists -> skip the download and only parse the file from the storage again
                    addEvents(false, false);
                    parseFile(context, true, false);
                }
            }
        }).start();
    }

    /**
     * parses the downloaded file and adds the events to the calendar
     *
     * @param context     Context
     * @param isUpdate    true if the parsing "updates" existing events, false if the parsing adds new events
     * @param isAutomatic true if the method is called via the automatic download, false if the method is started by the user
     */
    public void parseFile(final Context context, final boolean isUpdate, final boolean isAutomatic) {
        Log.d(TAG, this.getClass().getSimpleName() + ": " + "parse file");
        // get the parsing by calling parseFile on the parser
        String parsedFile = parser.parseFile();

        final boolean parsedFileEmpty = parsedFile.equals("");
        readPreferences();
        if (parsedFileEmpty) {
            Log.d(TAG, this.getClass().getSimpleName() + ": " + "CAN'T DOWNLOAD FILE");
        } else {
            // generate the events
            parser.generateEvents(parsedFile);

            // create the summary for every event by reading the values of the summary and description of the VEvent
            String summary, description = "";
            for (WEvent we : parser.getwEventList()) {
                VEvent event = we.getvEvent();
                summary = we.getvEvent().getSummary().getValue();
                if (event.getDescription() != null)
                    description = event.getDescription().getValue();
                if (!description.equals(""))
                    description = "\n" + description;
                // add the event with all parameters to the calendar
                calendarFunctions.addEvent(we.getYear(), we.getMonth(), we.getDay(), we.getHour(), we.getMin(), we.getWasteType(), summary + description);
            }
        }

        Log.d(TAG, this.getClass().getSimpleName() + ": " + "added " + parser.getwEventList().size() + " events");

        // stop the process if it's part of the automatic download
        if (isAutomatic) {
            automaticUpdateNotification(parser.getwEventList().size());
        } else {
            if (mainActivityContext instanceof Activity) {
                // inform the user
                ((Activity) mainActivityContext).runOnUiThread(
                        new Runnable() {
                            @Override
                            public void run() {
                                if (parsedFileEmpty || parser.getwEventList().size() == 0) {
                                    if (isUpdate) // updating events failed
                                        Toast.makeText(context, resources.getString(R.string.noUpdate), Toast.LENGTH_LONG).show();
                                    else // adding events failed
                                        Toast.makeText(context, resources.getString(R.string.noFileDownloaded), Toast.LENGTH_LONG).show();

                                } else {
                                    if (isUpdate) // updating events successful
                                        Toast.makeText(context, resources.getString(R.string.updatedEventsPt1) + " " + parser.getwEventList().size() + " " + resources.getString(R.string.updatedNewEventsPt2), Toast.LENGTH_LONG).show();
                                    else // adding events successful
                                        Toast.makeText(context, resources.getString(R.string.addedNewEventsPt1) + " " + parser.getwEventList().size() + " " + resources.getString(R.string.addedNewEventsPt2), Toast.LENGTH_LONG).show();
                                }
                                updateAddEventsBtn(true, resources.getString(R.string.btnEvents));
                            }
                        });
            }
        }
    }

    /**
     * creates a notification for the automatic update
     * for source see header of this file
     *
     * @param numberOfUpdatedEvents number of updated events to determine failure or success of the download
     */
    private void automaticUpdateNotification(int numberOfUpdatedEvents) {
        if (notification) {
            createNotificationChannel();
            Intent intent;
            PendingIntent pendingIntent;

            if (numberOfUpdatedEvents == 0) {
                // Create an Intent for the activity you want to start
                intent = new Intent(context, SettingsActivity.class);
                // Create the TaskStackBuilder and add the intent, which inflates the back stack
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                stackBuilder.addNextIntentWithParentStack(intent);
                // Get the PendingIntent containing the entire back stack
                pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            } else {
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("content://com.android.calendar/time/" + System.currentTimeMillis()));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
            }

            int notificationId = 1;

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_notification)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setContentTitle(resources.getString(R.string.notificationTitle))
                    // Set the intent that will fire when the user taps the notification
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true);

            if (numberOfUpdatedEvents == 0) {
                mBuilder.setContentText(resources.getString(R.string.failedNotificationDesc));
            } else {
                mBuilder.setContentText(resources.getString(R.string.updatedNotificationDesc));
            }

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

            // notificationId is a unique int for each notification that you must define
            notificationManager.notify(notificationId, mBuilder.build());
        }
    }

    /**
     * creates a notification channel.
     * for source see header of this file
     */
    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "channel name";//resources.getString(R.string.channel_name);
            String description = "channel desc";//resources.getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    /**
     * returns the requested permission state
     *
     * @param permission   requested permission
     * @param callBackCode callback code of the permission
     * @return true if permission granted, else false
     */
    private boolean getPermission(String permission, int callBackCode) {
        Context thisContext = mainActivityContext;
        if (context instanceof Activity) thisContext = context;
        if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) thisContext, permission)) {
                return false;
            } else {
                ActivityCompat.requestPermissions((Activity) thisContext, new String[]{permission}, callBackCode);
                return false;
            }
        }
        return true;
    }
}
