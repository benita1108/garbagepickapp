/*
 * This project is licensed under Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0) except as noted.
 * Copyright (c) 2019 Benita Ludwig
 *
 * All third party material is sourced in the applicable file.
 * For full license see LICENSE.txt, NOTICE.txt and LICENSE-THIRD-PARTY.txt.
 */

package com.ludwig.benita.garbagepickapp;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;
import android.util.Log;

import com.ludwig.benita.garbagepickapp.parsing.WasteType;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import static com.ludwig.benita.garbagepickapp.MainActivity.TAG;

/**
 * class that provides all the functions related to the calendar.
 * <p>
 * <pre>
 * Source:
 * CalendarFunctions.java
 *     This class includes code that is based on a tutorial by Wolfram Rittmeyer from Grokking Android.
 *         tutorial: https://www.grokkingandroid.com/androids-calendarcontract-provider/
 *             with written permission to use this code in this app.
 *     Copyright 2012 © Grokking Android
 * </pre>
 *
 * @author Benita Ludwig
 */
public class CalendarFunctions {

    /**
     * whole address for the event
     */
    private String address = "";
    /**
     * preference for notifications
     */
    private boolean notifications = false;
    /**
     * number of days before the event for the notification
     */
    private int notificationDaysAhead = 0;
    /**
     * time of the notification
     */
    private long notificationTime = 0;
    /**
     * repetition interval for the notifications
     */
    private long notificationRepeat = 0;

    private ContentResolver resolver;
    private Context context;

    /**
     * constructor that sets the ContentResolver and Context
     *
     * @param resolver ContentResolver
     * @param context  Context
     */
    public CalendarFunctions(ContentResolver resolver, Context context) {
        this.resolver = resolver;
        this.context = context;
    }

    /**
     * sets the address string
     *
     * @param address given address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * sets the notifications
     *
     * @param notifications true if notifications are enabled in the settings, else false
     */
    void setNotifications(boolean notifications) {
        this.notifications = notifications;
    }

    /**
     * sets the day count for the notification of the event
     *
     * @param notificationDaysAhead number of days the notification should start before the event
     */
    void setNotificationDaysAhead(int notificationDaysAhead) {
        this.notificationDaysAhead = notificationDaysAhead;
    }

    /**
     * sets the time for the notification of the event
     *
     * @param notificationTime time (hr:min of the day) in millis for the notification to start
     */
    void setNotificationTime(long notificationTime) {
        this.notificationTime = notificationTime;
    }

    /**
     * sets the repeating time for the notification of the event
     *
     * @param notificationRepeat time in millis as the repeating cycle of the notification
     */
    void setNotificationRepeat(long notificationRepeat) {
        this.notificationRepeat = notificationRepeat;
    }

    /**
     * deletes all events of the given WasteType
     *
     * @param wasteType WasteType of the to-be-deleted events
     */
    public void deleteAllEvents(WasteType wasteType) {
        if (wasteType == null) return;
        // get the calendarId of the given WasteType
        long calId = getCalendarId(wasteType);
        resolver.delete(
                CalendarContract.Events.CONTENT_URI,
                CalendarContract.Events.CALENDAR_ID + " =? ",
                new String[]{Long.toString(calId)});
    }

    /**
     * creates a calendar for the given WasteType
     *
     * @param wasteType WasteType of the calendar
     */
    void createCalendar(WasteType wasteType) {
        if (wasteType == null) return;
        // empty calendar if it exists already
        deleteAllEvents(wasteType);
        // get the calendarId of the given WasteType
        long calId = getCalendarId(wasteType);
        if (calId != -1) {
            Log.d(TAG, this.getClass().getSimpleName() + ": " + "CALENDAR FOR " + wasteType.name() + " EXISTS ALREADY WITH ID " + calId);
            return;
        }

        // initialize the calendar with needed data
        ContentValues values = new ContentValues();
        values.put(CalendarContract.Calendars.ACCOUNT_NAME, context.getString(R.string.calendar_display_name));
        values.put(CalendarContract.Calendars.ACCOUNT_TYPE, CalendarContract.ACCOUNT_TYPE_LOCAL);
        values.put(CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL, CalendarContract.Calendars.CAL_ACCESS_OWNER);
        values.put(CalendarContract.Calendars.CALENDAR_TIME_ZONE, TimeZone.getDefault().getID());
        values.put(CalendarContract.Calendars.SYNC_EVENTS, 1);
        // calendar gets the name and color of the WasteType
        values.put(CalendarContract.Calendars.NAME, wasteType.toString());
        values.put(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME, WasteType.getTitle(wasteType, context));
        values.put(CalendarContract.Calendars.CALENDAR_COLOR, WasteType.getColor(wasteType, context));

        Uri.Builder builder = CalendarContract.Calendars.CONTENT_URI.buildUpon();
        builder.appendQueryParameter(CalendarContract.Calendars.ACCOUNT_NAME, context.getString(R.string.calendar_display_name));
        builder.appendQueryParameter(CalendarContract.Calendars.ACCOUNT_TYPE, CalendarContract.ACCOUNT_TYPE_LOCAL);
        builder.appendQueryParameter(CalendarContract.CALLER_IS_SYNCADAPTER, "true");
        // insert calendar
        resolver.insert(builder.build(), values);
    }

    /**
     * returns the calendar id from a given WasteType
     *
     * @param wasteType WasteType of the calendar
     * @return id of the calendar, returns -1 when the calendar can't be found
     */
    long getCalendarId(WasteType wasteType) {
        if (wasteType == null) wasteType = WasteType.DEFAULT;

        long id = -1;
        String[] projection = new String[]{CalendarContract.Calendars._ID};
        String selection = CalendarContract.Calendars.ACCOUNT_NAME + " = ? AND " +
                CalendarContract.Calendars.ACCOUNT_TYPE + " = ? AND " +
                CalendarContract.Calendars.NAME + " = ? ";
        String[] selArgs =
                new String[]{context.getString(R.string.calendar_display_name), CalendarContract.ACCOUNT_TYPE_LOCAL, wasteType.toString()};
        Cursor cursor = resolver.query(
                CalendarContract.Calendars.CONTENT_URI,
                projection, selection, selArgs, null);
        if (cursor != null && cursor.moveToFirst()) {
            // calendar was found
            id = cursor.getLong(0);
            if (!cursor.isClosed())
                cursor.close();
        }
        return id;
    }

    /**
     * deletes the calendar from the given WasteType
     *
     * @param wasteType WasteType of the to-be-deleted calendar
     */
    public void deleteCalendar(WasteType wasteType) {
        String selection = CalendarContract.Calendars.ACCOUNT_NAME + " = ? AND " +
                CalendarContract.Calendars.ACCOUNT_TYPE + " = ? AND " +
                CalendarContract.Calendars.NAME + " = ? ";
        String[] selArgs =
                new String[]{context.getString(R.string.calendar_display_name), CalendarContract.ACCOUNT_TYPE_LOCAL, wasteType.toString()};
        resolver.delete(
                CalendarContract.Calendars.CONTENT_URI,
                selection, selArgs);
        Log.d(TAG, this.getClass().getSimpleName() + ": " + "CALENDAR FOR " + wasteType.name() + " SUCCESSFULLY DELETED");
    }

    /**
     * adds an event with an empty summary to the given WasteType calendar
     *
     * @param year      year of the event
     * @param month     month of the event
     * @param day       day of the event
     * @param hour      hour of the event
     * @param min       minute of the event
     * @param wasteType WasteType of the event
     */
    void addEvent(int year, int month, int day, int hour, int min, WasteType wasteType) {
        addEvent(year, month, day, hour, min, wasteType, "");
    }

    /**
     * adds an event to the given WasteType calendar
     *
     * @param year      year of the event
     * @param month     month of the event
     * @param day       day of the event
     * @param hour      hour of the event
     * @param min       minute of the event
     * @param wasteType WasteType of the event
     * @param summary   summary of the event
     */
    void addEvent(int year, int month, int day, int hour, int min, WasteType wasteType, String summary) {
        if (wasteType == null) wasteType = WasteType.DEFAULT;
        // get the calendarId of the given WasteType
        long calId = getCalendarId(wasteType);
        if (calId == -1) {
            Log.d(TAG, this.getClass().getSimpleName() + ": " + "CAN'T FIND CALENDAR FOR THIS EVENT.");
            return;
        }

        // create Calendar-object with the event info to generate the time in millis
        Calendar cal = new GregorianCalendar(year, month, day);
        // the default hour of the calendar is 6 am
        cal.set(Calendar.HOUR_OF_DAY, 6);
        if (hour != 0) cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, min);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        long startTimeInMillis = cal.getTimeInMillis();

        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.DTSTART, startTimeInMillis);
        // set the length of the event to 1 hr
        values.put(CalendarContract.Events.DTEND, startTimeInMillis + 3600000);
        // alternatively set the event to an all-day event
        //values.put(CalendarContract.Events.ALL_DAY, 1);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().getID());
        // if an address exists, add it to the location of the event
        if (!address.equals("   "))
            values.put(CalendarContract.Events.EVENT_LOCATION, address);
        values.put(CalendarContract.Events.CALENDAR_ID, calId);
        values.put(CalendarContract.Events.ACCESS_LEVEL, CalendarContract.Events.ACCESS_PRIVATE);
        values.put(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);

        // add the info of the WasteType to the event
        int color = WasteType.getColor(wasteType, context);
        String title = WasteType.getTitle(wasteType, context);
        String description = WasteType.getDesc(wasteType, context);
        if (!summary.equals("")) description = summary;
        // add the last update info to the event
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy (HH:mm:ss)");
        description = description + "\n" + context.getResources().getString(R.string.updated) + ": " + dateFormat.format(new Date());

        values.put(CalendarContract.Events.EVENT_COLOR, color);
        values.put(CalendarContract.Events.TITLE, title);
        values.put(CalendarContract.Events.DESCRIPTION, description);

        long eventId = -1;
        // insert event
        Uri uri = resolver.insert(CalendarContract.Events.CONTENT_URI, values);
        if (uri != null) {
            String lastPathSegment = uri.getLastPathSegment();
            if (lastPathSegment != null)
                eventId = Long.valueOf(lastPathSegment);
        }

        // start generating notifications for the event if the setting is enabled
        if (notifications)
            generateNotifications(eventId, startTimeInMillis);
    }

    /**
     * generates notifications for the given event
     *
     * @param eventId           id of the event
     * @param startTimeInMillis start time in millis of the event
     */
    private void generateNotifications(long eventId, long startTimeInMillis) {
        Calendar calNotif = Calendar.getInstance();
        // calculate the time in millis for the notification based on the days-ahead-value
        long daysAheadInMillis = notificationDaysAhead * (86400 * 1000);
        // set the time of the Calendar-object exactly [notificationDaysAhead] days before the start of the event
        calNotif.setTimeInMillis(startTimeInMillis - daysAheadInMillis);

        Calendar calTime = Calendar.getInstance();
        // set another Calendar-object to the time (hr:min) of the notification
        calTime.setTimeInMillis(notificationTime);
        // get the hour and minute of this calendar and add the times to the notification Calendar-object
        calNotif.set(Calendar.HOUR_OF_DAY, calTime.get(Calendar.HOUR_OF_DAY));
        calNotif.set(Calendar.MINUTE, calTime.get(Calendar.MINUTE));
        // notifInMillis refers to the timestamp of the notification in millis
        long notifInMillis = calNotif.getTimeInMillis();

        ContentValues values = new ContentValues();
        // from the timestamp of the notification to the start time of the event: add a notification every [notificationRepeat]
        // notificationRepeat is the notification cycle in millis
        for (long notifAndRepeat = notifInMillis; notifAndRepeat <= startTimeInMillis; notifAndRepeat += notificationRepeat) {

            // calculate how many minutes before the even the notification should appear
            long calculatedMinutes = startTimeInMillis - notifAndRepeat;
            // if notification timestamp is after event start, set minutes to 0
            if (calculatedMinutes < 0) calculatedMinutes = 0;
            calculatedMinutes = calculatedMinutes / 1000 / 60;

            // add values to the event
            values.put(CalendarContract.Reminders.EVENT_ID, eventId);
            values.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
            values.put(CalendarContract.Reminders.MINUTES, calculatedMinutes);
            resolver.insert(CalendarContract.Reminders.CONTENT_URI, values);
            // if no repetition is wanted, cancel the calculation of the repeating notifications
            if (notificationRepeat == -1)
                break;
        }
    }
}
