/*
 * This project is licensed under Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0) except as noted.
 * Copyright (c) 2019 Benita Ludwig
 *
 * All third party material is sourced in the applicable file.
 * For full license see LICENSE.txt, NOTICE.txt and LICENSE-THIRD-PARTY.txt.
 */

package com.ludwig.benita.garbagepickapp.parsing;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;

import com.ludwig.benita.garbagepickapp.NetworkConnection;
import com.ludwig.benita.garbagepickapp.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.TimeZone;

import biweekly.Biweekly;
import biweekly.ICalendar;
import biweekly.component.VEvent;
import biweekly.io.TimezoneAssignment;
import biweekly.io.TimezoneInfo;
import biweekly.util.UtcOffset;

/**
 * abstract class that provides all the methods related to the parsing of the ics-file.
 * <p>
 * <pre>
 * This project uses the library biweekly and its example code by Michael Angstadt.
 * https://github.com/mangstadt/biweekly
 *     under license: BSD 2-Clause
 * Copyright (c) 2013-2018, Michael Angstadt
 * </pre>
 *
 * @author Benita Ludwig
 */
public abstract class IcsParser {

    /**
     * name under which the event file should be saved
     */
    public static final String FILENAME = "garbage-pick-app-events.ics";
    /**
     * list of WasteTypes that are supported by this parser
     */
    ArrayList<WasteType> allAvailableWasteTypes;
    /**
     * list of WEvents that got parsed
     */
    ArrayList<WEvent> wEventList;
    private Context context;
    /**
     * preference for downloads via mobile data
     */
    private boolean networkMobile = false;
    /**
     * preference for downloads via wifi
     */
    private boolean networkWifi = true;

    /**
     * constructor that sets the Context
     *
     * @param context Context
     */
    IcsParser(Context context) {
        this.context = context;
        wEventList = new ArrayList<>();
        allAvailableWasteTypes = new ArrayList<>();
    }

    /**
     * returns all available WasteTypes for this parser
     *
     * @return list of all available WasteTypes
     */
    public List<WasteType> getAllAvailableWasteTypes() {
        return allAvailableWasteTypes;
    }

    /**
     * sets network connection via mobile data
     *
     * @param networkMobile true if downloads via mobile data are allowed, else false
     */
    public void setNetworkMobile(boolean networkMobile) {
        this.networkMobile = networkMobile;
    }

    /**
     * sets network connection via wifi
     *
     * @param networkWifi true if downloads via wifi are allowed, else false
     */
    public void setNetworkWifi(boolean networkWifi) {
        this.networkWifi = networkWifi;
    }

    /**
     * returns a list of WEvents that are parsed from the file
     *
     * @return list of WEvents
     */
    public ArrayList<WEvent> getwEventList() {
        return wEventList;
    }

    /**
     * starts the downloading process for the file
     *
     * @return id of the download
     */
    public abstract long downloadFile();

    /**
     * downloads the file from a given url and returns the id of the download
     *
     * @param url url from where the file should be downloaded
     * @return id of the download
     */
    long downloadFile(String url) {

        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            Toast.makeText(context, context.getResources().getString(R.string.noSdCard), Toast.LENGTH_LONG).show();
            return -1;
        }

        // create a new file
        File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), FILENAME);
        Uri fileUri = Uri.fromFile(file);

        // delete file if it exists already
        // (else the new file gets saved under a different name and can't be parsed)
        if (file.exists()) file.delete();

        // parse uri from url
        Uri urlUri = Uri.parse(url);

        // create a new DownloadManager Request from the uri
        DownloadManager.Request request = new DownloadManager.Request(urlUri);
        // check if device is currently connected to the necessary networks
        NetworkConnection nwc = new NetworkConnection(context);
        // if mobile allowed and wifi not allowed OR both allowed and no wifi connected
        if ((networkMobile && !networkWifi) || (networkMobile && networkWifi && !nwc.connectedWifi())) {
            // use mobile data for download
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE);
        }
        // if wifi allowed and wifi connected
        if (networkWifi && nwc.connectedWifi()) {
            // use wifi for download
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
        }
        // additional settings for the download
        request.setAllowedOverRoaming(false)
                .setTitle(context.getResources().getString(R.string.downloadTitle))
                .setDescription(context.getResources().getString(R.string.downloadDesc))
                // set visibility
                .setVisibleInDownloadsUi(false)
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
                .setDestinationUri(fileUri);

        DownloadManager downloadmanager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        // add requested download to the DownloadManager queue
        long download = downloadmanager.enqueue(request);
        return download;
    }

    /**
     * generates the download url from the address and returns it
     *
     * @return the url for the download
     */
    public abstract String generateURL();

    /**
     * parses the downloaded file
     *
     * @return the parsing as a string
     */
    public String parseFile() {
        String str = "";
        File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), FILENAME);
        // if file doesn't exist
        if (!file.exists()) {
            // return empty string
            return str;
        }

        // get path of file
        String path = file.getAbsolutePath();
        FileInputStream fin = null;
        try {
            fin = new FileInputStream(path);
            // read the InputStream of the file with a BufferedReader
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fin));
            try {
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line);
                    stringBuilder.append("\n");
                }
                str = stringBuilder.toString();
            } finally {
                bufferedReader.close();
            }
            return str;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            // file not found
            return str;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fin != null) fin.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return str;
    }

    /**
     * generates the events from the parsed string
     *
     * @param str string of the parsing
     */
    public void generateEvents(String str) {
        // parse the string and create a list of ICalendar elements
        List<ICalendar> icals = Biweekly.parse(str).all();
        wEventList.clear();
        long utcOffsetInMillis = 0;

        // go through all ICalendar-objects in icals
        for (ICalendar ical : icals) {
            List<VEvent> list = ical.getEvents();
            Collection<TimezoneAssignment> timezones = ical.getTimezoneInfo().getTimezones();
            // if no timezone is assigned to the events, add London as UTC+0 timezone
            if (timezones.isEmpty()) {
                TimezoneInfo tzi = new TimezoneInfo();
                TimezoneAssignment timezoneAssignment = new TimezoneAssignment(TimeZone.getTimeZone("Europe/London"), "Europe/London");
                tzi.setDefaultTimezone(timezoneAssignment);
                ical.setTimezoneInfo(tzi);
                timezones.add(timezoneAssignment);
            }
            // get the offset of the timezone for the events
            for (TimezoneAssignment timezoneAssignment : timezones) {
                UtcOffset utcOffset = UtcOffset.parse(timezoneAssignment.getTimeZone());
                utcOffsetInMillis = utcOffset.getMillis();
                WEvent.setUtcOffsetInMillis(utcOffsetInMillis);
            }

            // go through all the vEvents
            for (int i = 0; i < list.size(); i++) {
                VEvent vEvent = list.get(i);
                WEvent wEvent = new WEvent(vEvent);
                wEventList.add(wEvent);
            }
            generateWasteTypes();
        }
    }

    /**
     * generates the WasteType of each event based on the name of the WasteType in the parsed file
     */
    public abstract void generateWasteTypes();
}
