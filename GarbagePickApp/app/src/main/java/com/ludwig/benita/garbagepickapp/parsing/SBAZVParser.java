/*
 * This project is licensed under Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0) except as noted.
 * Copyright (c) 2019 Benita Ludwig
 *
 * All third party material is sourced in the applicable file.
 * For full license see LICENSE.txt, NOTICE.txt and LICENSE-THIRD-PARTY.txt.
 */

package com.ludwig.benita.garbagepickapp.parsing;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

/**
 * class that parses files from SBAZV (Südbrandenburgischer Abfallzweckverband)
 * https://www.sbazv.de/
 *
 * @author Benita Ludwig
 */
public class SBAZVParser extends IcsParser {

    /**
     * name of the WasteType PAPER in the parsed file
     */
    private static final String PAPER = "Papiertonnen";
    /**
     * name of the WasteType RECYCLING in the parsed file
     */
    private static final String RECYCLING = "Gelbe Säcke";
    /**
     * name of the WasteType RESIDUAL in the parsed file
     */
    private static final String RESIDUAL = "Restmülltonnen";
    /**
     * name of the WasteType CHRISTMASTREE in the parsed file
     */
    private static final String CHRISTMASTREE = "Weihnachtsbäume";
    /**
     * name of the WasteType LEAVES in the parsed file
     */
    private static final String LEAVES = "Laubsäcke";
    //private static final String RESIDUAL_CONTAINER = "";
    //private static final String ORGANIC = "";

    private SharedPreferences sharedPreferences;

    /**
     * constructor that sets the Context and SharedPreferences
     *
     * @param context           Context
     * @param sharedPreferences SharedPreferences
     */
    public SBAZVParser(Context context, SharedPreferences sharedPreferences) {
        super(context);
        this.sharedPreferences = sharedPreferences;
        // add supported WasteTypes to allAvailableWasteTypes
        allAvailableWasteTypes.add(WasteType.PAPER);
        allAvailableWasteTypes.add(WasteType.RECYCLING);
        allAvailableWasteTypes.add(WasteType.RESIDUAL);
        allAvailableWasteTypes.add(WasteType.CHRISTMASTREE);
        allAvailableWasteTypes.add(WasteType.LEAVES);

        generateURL();
    }

    @Override
    public long downloadFile() {
        return super.downloadFile(generateURL());
    }

    @Override
    public String generateURL() {
        // get the city name from the preferences
        String city = sharedPreferences.getString("pref_addr_city", "");
        if (city == null) city = "";
        // replace all spaces with plus
        city = city.replaceAll(" ", "+");

        // get the district name from the preferences
        String district = sharedPreferences.getString("pref_addr_district", "");
        if (district == null) district = "";
        // if the district name is empty, use city as district
        if (district.equals("") || district.equals(" ")) district = city;
        // replace all spaces with plus
        district = district.replaceAll(" ", "+");

        // get the street name from the preferences
        String street = sharedPreferences.getString("pref_addr_street", "");
        if (street == null) street = "";
        // replace all spaces with plus and shorten 'traße' to 'tr.'
        street = street.replaceAll(" ", "+").replaceAll("traße", "tr.");

        // url example: http://www.sbazv.de/entsorgungstermine/klein.ics?city=CITY&district=DISTRICT&street=STREET
        // generate the url
        String url = "http://www.sbazv.de/entsorgungstermine/klein.ics?city=" + city + "&district=" + district + "&street=" + street;
        // check api level >= 25
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1)
            url = "https://www.sbazv.de/entsorgungstermine/klein.ics?city=" + city + "&district=" + district + "&street=" + street;
        // Log.d(TAG, this.getClass().getSimpleName() + ": " + "SBAZV URL: " + url);
        return url;
    }

    @Override
    public void generateWasteTypes() {
        // go through the WEvent list
        for (WEvent ev : super.wEventList) {
            // read the summary of the WEvent
            String summary = ev.getvEvent().getSummary().getValue();
            // check for the appearance of a WasteType name in the event and set the corresponding WasteType
            if (summary.contains(PAPER)) ev.setWasteType(WasteType.PAPER);
            if (summary.contains(RECYCLING)) ev.setWasteType(WasteType.RECYCLING);
            if (summary.contains(RESIDUAL)) ev.setWasteType(WasteType.RESIDUAL);
            if (summary.contains(CHRISTMASTREE)) ev.setWasteType(WasteType.CHRISTMASTREE);
            if (summary.contains(LEAVES)) ev.setWasteType(WasteType.LEAVES);
        }
    }
}
