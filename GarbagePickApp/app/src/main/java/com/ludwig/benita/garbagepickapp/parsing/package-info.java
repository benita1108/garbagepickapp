/**
 * package that contains the main class for parsing matters (IcsParser),
 * all implemented parsers and additional classes and enums that are needed for the parsers
 *
 * @author Benita Ludwig
 */
package com.ludwig.benita.garbagepickapp.parsing;
